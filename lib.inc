%define SYS_EXIT 60
%define SYS_WRITE 1
%define STD_OUT 1
%define SYMBOL_EOL 0
%define DEC_FORMAT 10

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .iter:
        cmp byte[rdi + rax], SYMBOL_EOL
        je .quit
        inc rax
        jmp .iter
    .quit:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rax, SYS_WRITE
    mov rdi, STD_OUT
    mov rdx, 1
    syscall
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jnl print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, DEC_FORMAT
    push SYMBOL_EOL
    .loop:
        xor rdx, rdx
        div rcx
        add rdx, '0'
        push rdx
        test rax, rax
        jnz .loop
    .print:
        pop rdi
        test rdi, rdi
        jz .exit
        call print_char
        jmp .print
    .exit:
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .iter:
        mov dl, [rsi + rcx]
        cmp dl, [rdi + rcx]
        jne .not_equal
        inc rcx
        cmp dl, SYMBOL_EOL
        jne .iter
    mov rax, 1
    jmp .exit
    .not_equal:
        xor rax, rax
    .exit:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rdx, rdx
    .iter:
        push rdi
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        cmp rax, ' '
        je .skip
        cmp rax, `\t`
        je .skip
        cmp rax, `\n`
        je .skip
        cmp rdx, rsi
        jnb .error
        cmp rax, SYMBOL_EOL
        je .exit
        mov [rdi + rdx], al
        inc rdx
        jmp .iter
    .error:
        xor rax, rax
        ret
    .skip:
        cmp rdx, 0
        je .iter
    .exit:
        mov byte[rdi + rdx], SYMBOL_EOL
        mov rax, rdi
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor rsi, rsi
    .iter:
        mov sil, [rdi + rcx]
        cmp sil, SYMBOL_EOL
        je .exit
        sub sil, '0'
        cmp sil, DEC_FORMAT
        jnb .exit
        imul rax, DEC_FORMAT
        add rax, rsi
        inc rcx
        jmp .iter
    .exit:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    call parse_uint
    jmp .exit
    .negative:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
    .exit:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jb .exit
    xor rcx, rcx
    .iter:
        mov r8b, [rdi + rcx]
        mov [rsi + rcx], r8b
        cmp rcx, rax
        je .exit
        inc rcx
        jmp .iter
    .exit:
        xor rax, rax
        ret